package com.sample.openweather.ui.detail

import com.sample.openweather.AppComponent
import com.sample.openweather.di.scope.PerActivity
import dagger.Component

@PerActivity
@Component(
        dependencies = [(AppComponent::class)],
        modules = [(DetailModule::class)]
)
interface DetailComponent {

    fun injectTo(activity: DetailActivity)
}
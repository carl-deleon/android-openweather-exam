package com.sample.openweather.ui.main.button

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.openweather.R
import com.sample.openweather.RxBus
import com.sample.openweather.common.extension.inflateView
import kotlinx.android.synthetic.main.fragment_button.*

class ButtonFragment : Fragment() {

    companion object {
        val TAG: String = ButtonFragment::class.java.simpleName

        fun newInstance(): ButtonFragment = ButtonFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
            = container?.inflateView(R.layout.fragment_button, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refresh.setOnClickListener {
            RxBus.publish(RefreshClickEvent())
        }
    }
}
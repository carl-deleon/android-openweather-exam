package com.sample.openweather.ui.main.list

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.sample.openweather.data.entity.Weather

interface WeatherListContract {

    interface WeatherListPresenter : MvpPresenter<WeatherListView> {
        fun getList()
    }

    interface WeatherListView : MvpView {
        fun setData(weatherList: List<Weather>)

        fun showError(error: String?)
    }
}
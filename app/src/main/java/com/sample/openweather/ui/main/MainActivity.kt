package com.sample.openweather.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.sample.openweather.R
import com.sample.openweather.ui.main.button.ButtonFragment
import com.sample.openweather.ui.main.list.WeatherListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
    }

    private fun initViews() {
        if (supportFragmentManager.findFragmentByTag(WeatherListFragment.TAG) == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container_list, WeatherListFragment.newInstance(), WeatherListFragment.TAG)
                    .commit()
        }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container_button, ButtonFragment.newInstance(), ButtonFragment.TAG)
                .commit()
    }
}

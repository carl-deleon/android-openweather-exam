package com.sample.openweather.ui.detail

import com.sample.openweather.data.repository.WeatherRepository
import com.sample.openweather.di.scope.PerActivity
import dagger.Module
import dagger.Provides

@Module
class DetailModule {

    @PerActivity
    @Provides
    fun provideDetailPresenter(weatherRepository: WeatherRepository): DetailContract.DetailPresenter =
            DetailPresenterImpl(weatherRepository)
}
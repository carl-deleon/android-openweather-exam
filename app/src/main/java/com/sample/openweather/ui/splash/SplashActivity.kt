package com.sample.openweather.ui.splash

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.sample.openweather.R
import com.sample.openweather.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(openMainActivity(), 2000)
    }

    private fun openMainActivity() = Runnable {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
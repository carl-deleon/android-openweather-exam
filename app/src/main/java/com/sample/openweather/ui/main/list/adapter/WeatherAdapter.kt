package com.sample.openweather.ui.main.list.adapter

import android.annotation.SuppressLint
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.openweather.R
import com.sample.openweather.data.entity.Weather
import kotlinx.android.synthetic.main.layout_weather_item.view.*

class WeatherAdapter(
        private var weatherList: List<Weather>,
        private val itemClickListener: ItemClickListener
) : RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder>() {

    fun updateList(weatherList: List<Weather>) {
        this.weatherList = weatherList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: WeatherViewHolder?, position: Int) {
        holder?.bind(weatherList[position], itemClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): WeatherViewHolder = WeatherViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.layout_weather_item, parent, false))

    override fun getItemCount(): Int = weatherList.size

    class WeatherViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(weather: Weather, listener: ItemClickListener) = with(itemView) {
            tv_location.text = "${weather.location}, ${weather.country?.countryCode}"
            tv_weather_description.text = "${weather.description?.get(0)?.mainDesc} - ${weather.description?.get(0)?.description}"
            tv_temperature.text = String.format(resources.getString(R.string.format_temperature), weather.temp?.temperature)
            setOnClickListener {
                listener.onItemClicked(weather)
            }
        }
    }

    interface ItemClickListener {
        fun onItemClicked(weather: Weather)
    }
}
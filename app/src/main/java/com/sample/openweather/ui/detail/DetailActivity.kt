package com.sample.openweather.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.sample.openweather.R
import com.sample.openweather.common.extension.detailComponent
import com.sample.openweather.common.extension.formatString
import com.sample.openweather.data.entity.Weather
import com.sample.openweather.ui.detail.DetailContract.DetailPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import javax.inject.Inject

class DetailActivity : MvpActivity<DetailContract.DetailView, DetailPresenter>(), DetailContract.DetailView {

    @Inject lateinit var detailPresenter: DetailPresenter

    override fun createPresenter(): DetailPresenter = detailPresenter

    companion object {
        private val TAG: String = DetailActivity::class.java.simpleName
        val WEATHER_ID: String = TAG + ".weatherId"

        fun newIntent(context: Context?, weatherId: String): Intent =
                Intent(context, DetailActivity::class.java).apply {
                    putExtra(WEATHER_ID, weatherId)
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        detailComponent.injectTo(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }

        detailPresenter.getWeather(intent.getStringExtra(WEATHER_ID))
    }

    override fun setData(weather: Weather) {
        Picasso.with(this)
                .load(formatString(R.string.format_weather_icon, weather.description?.get(0)?.icon))
                .into(iv_weather_indicator)

        tv_location.text = formatString(R.string.format_location, weather.location, weather.country?.countryCode)
        tv_temperature.text = formatString(R.string.format_temperature, weather.temp?.temperature)
        tv_cloudiness.text = weather.description?.get(0)?.description
        tv_temp_min_max.text = formatString(R.string.format_temp_min_max, weather.temp?.minTemp, weather.temp?.maxTemp)
        tv_wind.text = formatString(R.string.format_wind, weather.wind?.speed, weather.wind?.degree)
        tv_pressure.text = formatString(R.string.format_pressure, weather.temp?.pressure)
        tv_humidity.text = formatString(R.string.format_humidity, weather.temp?.humidity)
        tv_coords.text = formatString(R.string.format_coords, weather.coordinates?.latitude, weather.coordinates?.longitude)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_refresh -> {
                presenter.refreshWeather()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
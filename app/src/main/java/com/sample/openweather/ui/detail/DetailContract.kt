package com.sample.openweather.ui.detail

import com.hannesdorfmann.mosby3.mvp.MvpPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import com.sample.openweather.data.entity.Weather

interface DetailContract {

    interface DetailView : MvpView {
        fun setData(weather: Weather)
    }

    interface DetailPresenter : MvpPresenter<DetailView> {
        fun getWeather(locationId: String)

        fun refreshWeather()
    }
}
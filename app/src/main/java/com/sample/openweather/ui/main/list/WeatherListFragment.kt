package com.sample.openweather.ui.main.list

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.hannesdorfmann.mosby3.mvp.MvpFragment
import com.sample.openweather.R
import com.sample.openweather.common.extension.inflateView
import com.sample.openweather.common.extension.mainComponent
import com.sample.openweather.data.entity.Weather
import com.sample.openweather.ui.detail.DetailActivity
import com.sample.openweather.ui.main.list.WeatherListContract.WeatherListPresenter
import com.sample.openweather.ui.main.list.WeatherListContract.WeatherListView
import com.sample.openweather.ui.main.list.adapter.WeatherAdapter
import kotlinx.android.synthetic.main.fragment_list.*
import java.util.*
import javax.inject.Inject

class WeatherListFragment : MvpFragment<WeatherListView, WeatherListPresenter>(), WeatherListView, WeatherAdapter.ItemClickListener {

    @Inject lateinit var listPresenter: WeatherListPresenter
    private val weatherAdapter: WeatherAdapter by lazy {
        WeatherAdapter(Collections.emptyList(), this@WeatherListFragment)
    }

    companion object {
        val TAG: String = WeatherListFragment::class.java.simpleName

        fun newInstance(): WeatherListFragment = WeatherListFragment()
    }

    override fun onAttach(context: Context?) {
        mainComponent.injectTo(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflateView(R.layout.fragment_list, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_view.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = weatherAdapter
        }
    }

    override fun createPresenter(): WeatherListPresenter {
        return listPresenter
    }

    override fun setData(weatherList: List<Weather>) {
        weatherAdapter.updateList(weatherList)
    }

    override fun showError(error: String?) {
        Toast.makeText(activity, error, Toast.LENGTH_SHORT).show()
    }

    override fun onItemClicked(weather: Weather) {
        val intent = DetailActivity.newIntent(activity, weather.id.toString())
        startActivity(intent)
    }
}
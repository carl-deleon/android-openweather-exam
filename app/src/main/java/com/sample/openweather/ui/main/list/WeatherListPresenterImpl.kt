package com.sample.openweather.ui.main.list

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.sample.openweather.RxBus
import com.sample.openweather.data.entity.Weather
import com.sample.openweather.data.entity.WeatherList
import com.sample.openweather.data.repository.WeatherRepository
import com.sample.openweather.ui.main.button.RefreshClickEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class WeatherListPresenterImpl(private val weatherRepository: WeatherRepository) : MvpBasePresenter<WeatherListContract.WeatherListView>(), WeatherListContract.WeatherListPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private var weatherList: List<Weather>? = null

    override fun attachView(view: WeatherListContract.WeatherListView?) {
        super.attachView(view)

        weatherList?.let { list -> getView().setData(list) } ?: getList()

        compositeDisposable.add(RxBus.listen(RefreshClickEvent::class.java)
                .subscribe({
                    getList()
                }))
    }

    override fun getList() {
        compositeDisposable.add(weatherRepository
                .getWeatherList(arrayOf("2643741", "3067696", "5391959"))
                .subscribeBy(
                        onNext = { weatherList ->
                            if (isViewAttached) {
                                view.setData(weatherList)
                                this@WeatherListPresenterImpl.weatherList = weatherList
                            }
                        },
                        onError = {
                            Timber.e(it)
                            if (isViewAttached) {
                                view.showError(it.message)
                            }
                        }
                ))
    }

    override fun detachView(retainInstance: Boolean) {
        if (!retainInstance) {
            compositeDisposable.clear()
        }
    }
}
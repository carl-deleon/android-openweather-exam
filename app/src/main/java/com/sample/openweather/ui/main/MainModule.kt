package com.sample.openweather.ui.main

import com.sample.openweather.data.repository.WeatherRepository
import com.sample.openweather.di.scope.PerFragment
import com.sample.openweather.ui.main.list.WeatherListContract
import com.sample.openweather.ui.main.list.WeatherListPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @PerFragment
    @Provides
    fun provideWeatherListPresenter(weatherRepository: WeatherRepository): WeatherListContract.WeatherListPresenter = WeatherListPresenterImpl(weatherRepository)
}
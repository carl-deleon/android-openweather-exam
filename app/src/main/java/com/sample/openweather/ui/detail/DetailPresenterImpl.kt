package com.sample.openweather.ui.detail

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.sample.openweather.data.repository.WeatherRepository
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class DetailPresenterImpl(private val weatherRepository: WeatherRepository) : MvpBasePresenter<DetailContract.DetailView>(), DetailContract.DetailPresenter {

    private lateinit var locationId: String

    override fun getWeather(locationId: String) {
        this.locationId = locationId

        refreshWeather()
    }

    override fun refreshWeather() {
        weatherRepository
                .getWeatherDescription(locationId)
                .subscribeBy(
                        onNext = { weather ->
                            if (isViewAttached) {
                                view.setData(weather)
                            }
                        },
                        onError = { throwable ->
                            Timber.e(throwable)
                        }
                )
    }
}
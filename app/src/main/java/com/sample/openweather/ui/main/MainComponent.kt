package com.sample.openweather.ui.main

import com.sample.openweather.AppComponent
import com.sample.openweather.di.scope.PerFragment
import com.sample.openweather.ui.main.list.WeatherListFragment
import dagger.Component

@PerFragment
@Component(
        dependencies = [(AppComponent::class)],
        modules = [(MainModule::class)]
)
interface MainComponent {

    fun injectTo(fragment: WeatherListFragment)
}
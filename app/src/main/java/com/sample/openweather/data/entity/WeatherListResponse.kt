package com.sample.openweather.data.entity

import android.arch.persistence.room.*
import com.google.gson.annotations.SerializedName
import com.sample.openweather.data.local.converter.ListConverter
import java.util.ArrayList

class WeatherList(
        @SerializedName("list") var weatherList: List<Weather>
)

@Entity(tableName = "tbl_weather")
class Weather(
        @PrimaryKey
        var id: Int = 0,
        @SerializedName("coord")
        @Embedded
        var coordinates: Coordinates? = null,
        @SerializedName("weather")
        var description: List<WeatherDesc>? = null,
        @SerializedName("main")
        @Embedded
        var temp: Temperature? = null,
        @Embedded
        var wind: Wind? = null,
        @SerializedName("sys")
        @Embedded
        var country: Country? = null,
        @SerializedName("name")
        var location: String = ""
)

class Coordinates(
        @SerializedName("lat") var latitude: Float,
        @SerializedName("lon") var longitude: Float
)

class Country(
        @SerializedName("country") var countryCode: String = ""
)

class WeatherDesc(
        @SerializedName("main") var mainDesc: String = "",
        var description: String = "",
        var icon: String = ""
)

class Temperature(
        @SerializedName("temp") var temperature: String = "",
        var pressure: String = "",
        var humidity: String = "",
        @SerializedName("temp_min") var minTemp: String = "",
        @SerializedName("temp_max") var maxTemp: String = ""
)

class Wind(
        var speed: String = "",
        @SerializedName("deg") var degree: String = ""
)
package com.sample.openweather.data.remote

import android.content.Context

import com.sample.openweather.data.exception.NoConnectivityException
import com.sample.openweather.util.AppUtil

import java.io.IOException

import okhttp3.Interceptor
import okhttp3.Response

class ConnectivityInterceptor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!AppUtil.isNetworkAvailable(context)) {
            throw NoConnectivityException()
        }
        return chain.proceed(chain.request())
    }
}
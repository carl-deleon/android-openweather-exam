package com.sample.openweather.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.sample.openweather.data.entity.Weather
import com.sample.openweather.data.local.converter.ListConverter

@Database(
        entities = [(Weather::class)],
        version = 1
)
@TypeConverters(ListConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun weatherDao(): WeatherDao
}
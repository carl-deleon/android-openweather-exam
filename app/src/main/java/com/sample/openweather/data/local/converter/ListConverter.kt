package com.sample.openweather.data.local.converter

import android.arch.persistence.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.sample.openweather.data.entity.WeatherDesc
import com.google.gson.Gson


class ListConverter {

    private val listType = object : TypeToken<List<WeatherDesc>>() {

    }.type

    @TypeConverter
    fun convertJsonToList(value: String): List<WeatherDesc> {
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun convertListToJson(list: List<WeatherDesc>): String {
        return Gson().toJson(list, listType)
    }
}
package com.sample.openweather.data.exception

class NoConnectivityException : RuntimeException {

    constructor() : super("No Internet Connection") {}

    constructor(message: String) : super(message) {}
}

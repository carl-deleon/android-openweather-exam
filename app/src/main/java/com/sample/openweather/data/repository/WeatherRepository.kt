package com.sample.openweather.data.repository

import com.sample.openweather.data.entity.Weather
import io.reactivex.Observable

interface WeatherRepository {

    fun getWeatherList(locationIds: Array<String>): Observable<List<Weather>>

    fun getWeatherDescription(locationId: String): Observable<Weather>
}
package com.sample.openweather.data.local

import android.arch.persistence.room.*
import com.sample.openweather.data.entity.Weather
import io.reactivex.Flowable

@Dao
interface WeatherDao {

    @Query("SELECT * FROM tbl_weather")
    fun getAll(): Flowable<List<Weather>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(weatherList: List<Weather>): List<Long>

    @Query("SELECT * FROM tbl_weather WHERE id = :cityId")
    fun getWeatherDetail(cityId: String): Flowable<Weather>

    @Update
    fun updateWeatherDetail(vararg weather: Weather)
}
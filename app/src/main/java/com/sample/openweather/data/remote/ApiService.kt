package com.sample.openweather.data.remote

import com.sample.openweather.data.entity.Weather
import com.sample.openweather.data.entity.WeatherList
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {

    @GET("group")
    fun getWeatherList(@QueryMap request: HashMap<String, String>): Observable<WeatherList>

    @GET("weather")
    fun getWeatherDesc(@QueryMap request: HashMap<String, String>): Observable<Weather>
}
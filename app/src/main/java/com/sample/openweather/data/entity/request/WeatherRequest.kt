package com.sample.openweather.data.entity.request

import com.sample.openweather.BuildConfig

data class WeatherRequest(
        private val locationIds: Array<String>
) {
    fun create(): HashMap<String, String> = hashMapOf(
            "id" to locationIds.joinToString(","),
            "units" to "metric",
            "APPID" to BuildConfig.API_KEY
    )
}

data class LocationWeatherRequest(
        private val locationId: String
) {
    fun create(): HashMap<String, String> = hashMapOf(
            "id" to locationId,
            "units" to "metric",
            "APPID" to BuildConfig.API_KEY
    )
}
package com.sample.openweather.data

import android.arch.persistence.room.Room
import android.content.Context
import com.sample.openweather.data.local.AppDatabase
import com.sample.openweather.data.remote.ApiService
import com.sample.openweather.data.remote.NetworkModule
import com.sample.openweather.data.repository.WeatherRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [(NetworkModule::class)])
class DataSourceModule {

    @Singleton
    @Provides
    fun provideAppDatabase(context: Context) =
            Room.databaseBuilder(context, AppDatabase::class.java, "db_openweather").build()

    @Singleton
    @Provides
    fun provideWeatherRepository(apiService: ApiService, appDatabase: AppDatabase): WeatherRepository {
        return DataSource(apiService, appDatabase)
    }
}
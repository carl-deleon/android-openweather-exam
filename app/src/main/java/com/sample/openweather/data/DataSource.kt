package com.sample.openweather.data

import com.sample.openweather.data.entity.Weather
import com.sample.openweather.data.entity.request.LocationWeatherRequest
import com.sample.openweather.data.entity.request.WeatherRequest
import com.sample.openweather.data.local.AppDatabase
import com.sample.openweather.data.remote.ApiService
import com.sample.openweather.data.repository.WeatherRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class DataSource(
        private val apiService: ApiService,
        private val appDatabase: AppDatabase
) : WeatherRepository {

    override fun getWeatherDescription(locationId: String): Observable<Weather> {
        val request = LocationWeatherRequest(locationId).create()

        return apiService.getWeatherDesc(request)
                .doOnNext({
                    appDatabase.weatherDao().updateWeatherDetail(it)
                })
                .subscribeOn(Schedulers.io())
                .onErrorResumeNext(Function { appDatabase.weatherDao().getWeatherDetail(locationId).toObservable() })
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getWeatherList(locationIds: Array<String>): Observable<List<Weather>> {
        val request = WeatherRequest(locationIds).create()

        return apiService.getWeatherList(request)
                .doOnNext({ weatherListResponse ->
                    val size = appDatabase.weatherDao().insertAll(weatherListResponse.weatherList)
                    Timber.d("$size items added")
                })
                .subscribeOn(Schedulers.io())
                .map { weatherListResponse -> weatherListResponse.weatherList }
                .onErrorResumeNext(Function { appDatabase.weatherDao().getAll().toObservable() })
                .observeOn(AndroidSchedulers.mainThread())
    }
}
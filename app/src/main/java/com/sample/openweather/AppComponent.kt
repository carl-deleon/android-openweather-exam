package com.sample.openweather

import android.content.Context
import com.sample.openweather.data.DataSourceModule
import com.sample.openweather.data.repository.WeatherRepository
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (DataSourceModule::class)])
interface AppComponent {

    fun context(): Context

    fun weatherRepository(): WeatherRepository
}
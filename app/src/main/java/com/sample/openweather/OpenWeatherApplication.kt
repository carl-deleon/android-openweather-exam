package com.sample.openweather

import android.app.Application
import com.sample.openweather.data.remote.NetworkModule
import timber.log.Timber

open class OpenWeatherApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    open val appComponent: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(BuildConfig.BASE_URL))
                .build()
    }
}
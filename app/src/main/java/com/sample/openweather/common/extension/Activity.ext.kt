package com.sample.openweather.common.extension

import android.app.Activity
import com.sample.openweather.AppComponent
import com.sample.openweather.OpenWeatherApplication
import com.sample.openweather.ui.detail.DaggerDetailComponent
import com.sample.openweather.ui.detail.DetailActivity
import com.sample.openweather.ui.detail.DetailComponent
import com.sample.openweather.ui.detail.DetailModule

val Activity.appComponent: AppComponent
    get() = (application as OpenWeatherApplication).appComponent

val DetailActivity.detailComponent: DetailComponent
    get() = DaggerDetailComponent.builder()
            .appComponent(appComponent)
            .detailModule(DetailModule())
            .build()

fun DetailActivity.formatString(stringResId: Int, vararg args: Any?): String =
        String.format(getString(stringResId), *args)
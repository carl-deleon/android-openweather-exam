package com.sample.openweather.common.extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.openweather.OpenWeatherApplication
import com.sample.openweather.ui.main.DaggerMainComponent
import com.sample.openweather.ui.main.MainComponent
import com.sample.openweather.ui.main.MainModule
import com.sample.openweather.ui.main.list.WeatherListFragment

fun ViewGroup.inflateView(layoutRes: Int, attachToRoot: Boolean): View?
        = LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

val WeatherListFragment.mainComponent: MainComponent
    get() = DaggerMainComponent.builder()
            .appComponent(activity?.appComponent)
            .mainModule(MainModule())
            .build()